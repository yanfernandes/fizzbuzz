defmodule FizzBuzz do
  def build(filename) do
    filename
    |> File.read()
    |> handler_file_read()
  end

  defp handler_file_read({:ok, result}) do
    result =
      result
      |> String.split(",")
      |> Enum.map(&convert_and_evaluate_numbers/1)

    {:ok, result}
  end

  defp handler_file_read({:error, reason}), do: "Error reading the file: #{reason}"

  defp convert_and_evaluate_numbers(elem) do
    elem
    |> String.to_integer()
    |> evaluate_numbers()
  end

  defp evaluate_numbers(number) when rem(number, 3) == 0 and rem(number, 5) == 0, do: :fizzbuzz
  defp evaluate_numbers(number) when rem(number, 3) == 0, do: :fizz
  defp evaluate_numbers(number) when rem(number, 5) == 0, do: :fizz
  defp evaluate_numbers(number), do: number
end
